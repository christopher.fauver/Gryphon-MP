"""the box menu provides varying services depending on box type."""
BACKGROUND_BOX = 1
TRACE_BOX = 2
REPORT_BOX = 3
TABLE_BOX = 4
GRAPH_BOX = 5
BAR_CHART_BOX = 6
GANTT_CHART_BOX = 7
AD_BOX = 8

BOX_NAMES = {
             BACKGROUND_BOX: "background",
             TRACE_BOX: "trace",
             REPORT_BOX: "report",
             TABLE_BOX: "table",
             GRAPH_BOX: "graph",
             BAR_CHART_BOX: "bar chart",
             GANTT_CHART_BOX: "Gantt chart",
             AD_BOX: "activity diagram",
            }
