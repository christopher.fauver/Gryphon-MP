# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settings_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.5.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QButtonGroup, QCheckBox,
    QDialog, QDialogButtonBox, QGroupBox, QLabel,
    QPushButton, QRadioButton, QSizePolicy, QTabWidget,
    QWidget)

from settings_slider import SettingsSlider
from settings_slider_percent import SettingsSliderPercent

class Ui_SettingsDialog(object):
    def setupUi(self, SettingsDialog):
        if not SettingsDialog.objectName():
            SettingsDialog.setObjectName(u"SettingsDialog")
        SettingsDialog.resize(682, 617)
        self.settings_widget = QTabWidget(SettingsDialog)
        self.settings_widget.setObjectName(u"settings_widget")
        self.settings_widget.setGeometry(QRect(10, 10, 661, 561))
        self.export_tab = QWidget()
        self.export_tab.setObjectName(u"export_tab")
        self.label_129 = QLabel(self.export_tab)
        self.label_129.setObjectName(u"label_129")
        self.label_129.setGeometry(QRect(30, 10, 501, 51))
        self.export_header_is_hidden_cb = QCheckBox(self.export_tab)
        self.export_header_is_hidden_cb.setObjectName(u"export_header_is_hidden_cb")
        self.export_header_is_hidden_cb.setGeometry(QRect(30, 100, 261, 24))
        self.export_background_is_transparent_cb = QCheckBox(self.export_tab)
        self.export_background_is_transparent_cb.setObjectName(u"export_background_is_transparent_cb")
        self.export_background_is_transparent_cb.setGeometry(QRect(30, 70, 471, 24))
        self.label_70 = QLabel(self.export_tab)
        self.label_70.setObjectName(u"label_70")
        self.label_70.setGeometry(QRect(30, 130, 161, 18))
        self.export_header_vertical_spacing_slider = SettingsSlider(self.export_tab)
        self.export_header_vertical_spacing_slider.setObjectName(u"export_header_vertical_spacing_slider")
        self.export_header_vertical_spacing_slider.setGeometry(QRect(190, 120, 200, 41))
        self.groupBox_18 = QGroupBox(self.export_tab)
        self.groupBox_18.setObjectName(u"groupBox_18")
        self.groupBox_18.setGeometry(QRect(10, 160, 151, 191))
        self.groupBox_18.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.export_image_format_bmp_rb = QRadioButton(self.groupBox_18)
        self.export_image_format_button_group = QButtonGroup(SettingsDialog)
        self.export_image_format_button_group.setObjectName(u"export_image_format_button_group")
        self.export_image_format_button_group.addButton(self.export_image_format_bmp_rb)
        self.export_image_format_bmp_rb.setObjectName(u"export_image_format_bmp_rb")
        self.export_image_format_bmp_rb.setGeometry(QRect(30, 30, 121, 24))
        self.export_image_format_jpg_rb = QRadioButton(self.groupBox_18)
        self.export_image_format_button_group.addButton(self.export_image_format_jpg_rb)
        self.export_image_format_jpg_rb.setObjectName(u"export_image_format_jpg_rb")
        self.export_image_format_jpg_rb.setGeometry(QRect(30, 60, 121, 24))
        self.export_image_format_pdf_rb = QRadioButton(self.groupBox_18)
        self.export_image_format_button_group.addButton(self.export_image_format_pdf_rb)
        self.export_image_format_pdf_rb.setObjectName(u"export_image_format_pdf_rb")
        self.export_image_format_pdf_rb.setGeometry(QRect(30, 90, 121, 24))
        self.export_image_format_png_rb = QRadioButton(self.groupBox_18)
        self.export_image_format_button_group.addButton(self.export_image_format_png_rb)
        self.export_image_format_png_rb.setObjectName(u"export_image_format_png_rb")
        self.export_image_format_png_rb.setGeometry(QRect(30, 120, 121, 24))
        self.export_image_format_svg_rb = QRadioButton(self.groupBox_18)
        self.export_image_format_button_group.addButton(self.export_image_format_svg_rb)
        self.export_image_format_svg_rb.setObjectName(u"export_image_format_svg_rb")
        self.export_image_format_svg_rb.setGeometry(QRect(30, 150, 121, 24))
        self.settings_widget.addTab(self.export_tab, "")
        self.background_tab = QWidget()
        self.background_tab.setObjectName(u"background_tab")
        self.label = QLabel(self.background_tab)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 60, 62, 17))
        self.background_color_pb = QPushButton(self.background_tab)
        self.background_color_pb.setObjectName(u"background_color_pb")
        self.background_color_pb.setGeometry(QRect(20, 80, 83, 41))
        self.background_color_gradient_slider = SettingsSliderPercent(self.background_tab)
        self.background_color_gradient_slider.setObjectName(u"background_color_gradient_slider")
        self.background_color_gradient_slider.setGeometry(QRect(130, 10, 200, 41))
        self.label_15 = QLabel(self.background_tab)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setGeometry(QRect(20, 20, 121, 20))
        self.settings_widget.addTab(self.background_tab, "")
        self.generation_tab = QWidget()
        self.generation_tab.setObjectName(u"generation_tab")
        self.groupBox_5 = QGroupBox(self.generation_tab)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.groupBox_5.setGeometry(QRect(30, 70, 171, 251))
        self.groupBox_5.setLayoutDirection(Qt.LeftToRight)
        self.groupBox_5.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.generation_charts_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_charts_use_border_cb.setObjectName(u"generation_charts_use_border_cb")
        self.generation_charts_use_border_cb.setGeometry(QRect(20, 180, 90, 23))
        self.generation_graphs_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_graphs_use_border_cb.setObjectName(u"generation_graphs_use_border_cb")
        self.generation_graphs_use_border_cb.setGeometry(QRect(20, 90, 90, 23))
        self.generation_reports_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_reports_use_border_cb.setObjectName(u"generation_reports_use_border_cb")
        self.generation_reports_use_border_cb.setGeometry(QRect(20, 120, 90, 23))
        self.generation_traces_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_traces_use_border_cb.setObjectName(u"generation_traces_use_border_cb")
        self.generation_traces_use_border_cb.setGeometry(QRect(20, 60, 90, 23))
        self.generation_background_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_background_use_border_cb.setObjectName(u"generation_background_use_border_cb")
        self.generation_background_use_border_cb.setGeometry(QRect(20, 30, 121, 23))
        self.generation_tables_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_tables_use_border_cb.setObjectName(u"generation_tables_use_border_cb")
        self.generation_tables_use_border_cb.setGeometry(QRect(20, 150, 90, 23))
        self.generation_ad_use_border_cb = QCheckBox(self.groupBox_5)
        self.generation_ad_use_border_cb.setObjectName(u"generation_ad_use_border_cb")
        self.generation_ad_use_border_cb.setGeometry(QRect(20, 210, 141, 23))
        self.label_33 = QLabel(self.generation_tab)
        self.label_33.setObjectName(u"label_33")
        self.label_33.setGeometry(QRect(30, 10, 431, 51))
        self.settings_widget.addTab(self.generation_tab, "")
        self.traces_tab = QWidget()
        self.traces_tab.setObjectName(u"traces_tab")
        self.groupBox = QGroupBox(self.traces_tab)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(10, 50, 631, 251))
        self.groupBox.setLayoutDirection(Qt.LeftToRight)
        self.groupBox.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(20, 130, 61, 20))
        self.label_6 = QLabel(self.groupBox)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(130, 130, 63, 20))
        self.label_7 = QLabel(self.groupBox)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(240, 130, 81, 20))
        self.label_8 = QLabel(self.groupBox)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(350, 130, 63, 20))
        self.label_9 = QLabel(self.groupBox)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setGeometry(QRect(460, 130, 63, 20))
        self.trace_node_use_border_cb = QCheckBox(self.groupBox)
        self.trace_node_use_border_cb.setObjectName(u"trace_node_use_border_cb")
        self.trace_node_use_border_cb.setGeometry(QRect(70, 90, 94, 26))
        self.trace_node_use_shadow_cb = QCheckBox(self.groupBox)
        self.trace_node_use_shadow_cb.setObjectName(u"trace_node_use_shadow_cb")
        self.trace_node_use_shadow_cb.setGeometry(QRect(180, 90, 94, 26))
        self.trace_node_root_color_pb = QPushButton(self.groupBox)
        self.trace_node_root_color_pb.setObjectName(u"trace_node_root_color_pb")
        self.trace_node_root_color_pb.setGeometry(QRect(20, 150, 83, 41))
        self.trace_node_atomic_color_pb = QPushButton(self.groupBox)
        self.trace_node_atomic_color_pb.setObjectName(u"trace_node_atomic_color_pb")
        self.trace_node_atomic_color_pb.setGeometry(QRect(130, 150, 83, 41))
        self.trace_node_composite_color_pb = QPushButton(self.groupBox)
        self.trace_node_composite_color_pb.setObjectName(u"trace_node_composite_color_pb")
        self.trace_node_composite_color_pb.setGeometry(QRect(240, 150, 83, 41))
        self.trace_node_schema_color_pb = QPushButton(self.groupBox)
        self.trace_node_schema_color_pb.setObjectName(u"trace_node_schema_color_pb")
        self.trace_node_schema_color_pb.setGeometry(QRect(350, 150, 83, 41))
        self.trace_node_say_color_pb = QPushButton(self.groupBox)
        self.trace_node_say_color_pb.setObjectName(u"trace_node_say_color_pb")
        self.trace_node_say_color_pb.setGeometry(QRect(460, 150, 83, 41))
        self.trace_node_color_gradient_slider = SettingsSliderPercent(self.groupBox)
        self.trace_node_color_gradient_slider.setObjectName(u"trace_node_color_gradient_slider")
        self.trace_node_color_gradient_slider.setGeometry(QRect(210, 200, 200, 41))
        self.label_14 = QLabel(self.groupBox)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setGeometry(QRect(110, 210, 111, 20))
        self.trace_node_height_slider = SettingsSlider(self.groupBox)
        self.trace_node_height_slider.setObjectName(u"trace_node_height_slider")
        self.trace_node_height_slider.setGeometry(QRect(70, 40, 200, 41))
        self.trace_node_width_slider = SettingsSlider(self.groupBox)
        self.trace_node_width_slider.setObjectName(u"trace_node_width_slider")
        self.trace_node_width_slider.setGeometry(QRect(70, 10, 200, 41))
        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(20, 50, 51, 20))
        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(20, 20, 51, 20))
        self.label_11 = QLabel(self.groupBox)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setGeometry(QRect(290, 50, 111, 20))
        self.label_12 = QLabel(self.groupBox)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setGeometry(QRect(290, 20, 131, 20))
        self.trace_node_v_spacing_slider = SettingsSlider(self.groupBox)
        self.trace_node_v_spacing_slider.setObjectName(u"trace_node_v_spacing_slider")
        self.trace_node_v_spacing_slider.setGeometry(QRect(420, 40, 200, 41))
        self.trace_node_h_spacing_slider = SettingsSlider(self.groupBox)
        self.trace_node_h_spacing_slider.setObjectName(u"trace_node_h_spacing_slider")
        self.trace_node_h_spacing_slider.setGeometry(QRect(420, 10, 200, 41))
        self.groupBox_2 = QGroupBox(self.traces_tab)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setGeometry(QRect(10, 310, 431, 211))
        self.groupBox_2.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.label_23 = QLabel(self.groupBox_2)
        self.label_23.setObjectName(u"label_23")
        self.label_23.setGeometry(QRect(20, 130, 91, 20))
        self.label_24 = QLabel(self.groupBox_2)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setGeometry(QRect(130, 130, 91, 20))
        self.label_25 = QLabel(self.groupBox_2)
        self.label_25.setObjectName(u"label_25")
        self.label_25.setGeometry(QRect(240, 130, 101, 20))
        self.trace_edge_in_color_pb = QPushButton(self.groupBox_2)
        self.trace_edge_in_color_pb.setObjectName(u"trace_edge_in_color_pb")
        self.trace_edge_in_color_pb.setGeometry(QRect(20, 150, 83, 41))
        self.trace_edge_follows_color_pb = QPushButton(self.groupBox_2)
        self.trace_edge_follows_color_pb.setObjectName(u"trace_edge_follows_color_pb")
        self.trace_edge_follows_color_pb.setGeometry(QRect(130, 150, 83, 41))
        self.trace_edge_user_defined_color_pb = QPushButton(self.groupBox_2)
        self.trace_edge_user_defined_color_pb.setObjectName(u"trace_edge_user_defined_color_pb")
        self.trace_edge_user_defined_color_pb.setGeometry(QRect(240, 150, 83, 41))
        self.trace_edge_arrow_size_slider = SettingsSlider(self.groupBox_2)
        self.trace_edge_arrow_size_slider.setObjectName(u"trace_edge_arrow_size_slider")
        self.trace_edge_arrow_size_slider.setGeometry(QRect(220, 20, 200, 41))
        self.label_22 = QLabel(self.groupBox_2)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setGeometry(QRect(20, 30, 91, 20))
        self.label_65 = QLabel(self.groupBox_2)
        self.label_65.setObjectName(u"label_65")
        self.label_65.setGeometry(QRect(20, 60, 161, 20))
        self.label_66 = QLabel(self.groupBox_2)
        self.label_66.setObjectName(u"label_66")
        self.label_66.setGeometry(QRect(20, 90, 251, 18))
        self.trace_edge_annotation_width_slider = SettingsSlider(self.groupBox_2)
        self.trace_edge_annotation_width_slider.setObjectName(u"trace_edge_annotation_width_slider")
        self.trace_edge_annotation_width_slider.setGeometry(QRect(220, 50, 200, 41))
        self.trace_edge_annotation_background_opacity_slider = SettingsSliderPercent(self.groupBox_2)
        self.trace_edge_annotation_background_opacity_slider.setObjectName(u"trace_edge_annotation_background_opacity_slider")
        self.trace_edge_annotation_background_opacity_slider.setGeometry(QRect(220, 80, 201, 41))
        self.label_2 = QLabel(self.traces_tab)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(20, 20, 151, 20))
        self.trace_hide_collapse_opacity_slider = SettingsSliderPercent(self.traces_tab)
        self.trace_hide_collapse_opacity_slider.setObjectName(u"trace_hide_collapse_opacity_slider")
        self.trace_hide_collapse_opacity_slider.setGeometry(QRect(180, 10, 200, 41))
        self.settings_widget.addTab(self.traces_tab, "")
        self.graphs_tab = QWidget()
        self.graphs_tab.setObjectName(u"graphs_tab")
        self.groupBox_3 = QGroupBox(self.graphs_tab)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.groupBox_3.setGeometry(QRect(10, 10, 631, 271))
        self.groupBox_3.setLayoutDirection(Qt.LeftToRight)
        self.groupBox_3.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.label_10 = QLabel(self.groupBox_3)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setGeometry(QRect(130, 160, 61, 20))
        self.graph_node_use_border_cb = QCheckBox(self.groupBox_3)
        self.graph_node_use_border_cb.setObjectName(u"graph_node_use_border_cb")
        self.graph_node_use_border_cb.setGeometry(QRect(70, 130, 94, 26))
        self.graph_node_color_pb = QPushButton(self.groupBox_3)
        self.graph_node_color_pb.setObjectName(u"graph_node_color_pb")
        self.graph_node_color_pb.setGeometry(QRect(130, 180, 83, 41))
        self.graph_node_color_gradient_slider = SettingsSliderPercent(self.groupBox_3)
        self.graph_node_color_gradient_slider.setObjectName(u"graph_node_color_gradient_slider")
        self.graph_node_color_gradient_slider.setGeometry(QRect(170, 220, 200, 41))
        self.label_17 = QLabel(self.groupBox_3)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setGeometry(QRect(70, 230, 101, 20))
        self.label_13 = QLabel(self.groupBox_3)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setGeometry(QRect(20, 50, 51, 20))
        self.label_16 = QLabel(self.groupBox_3)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setGeometry(QRect(20, 20, 51, 20))
        self.graph_node_width_slider = SettingsSlider(self.groupBox_3)
        self.graph_node_width_slider.setObjectName(u"graph_node_width_slider")
        self.graph_node_width_slider.setGeometry(QRect(70, 10, 200, 41))
        self.graph_node_height_slider = SettingsSlider(self.groupBox_3)
        self.graph_node_height_slider.setObjectName(u"graph_node_height_slider")
        self.graph_node_height_slider.setGeometry(QRect(70, 40, 200, 41))
        self.graph_node_v_spacing_slider = SettingsSlider(self.groupBox_3)
        self.graph_node_v_spacing_slider.setObjectName(u"graph_node_v_spacing_slider")
        self.graph_node_v_spacing_slider.setGeometry(QRect(420, 40, 200, 41))
        self.label_18 = QLabel(self.groupBox_3)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setGeometry(QRect(290, 20, 131, 20))
        self.graph_node_h_spacing_slider = SettingsSlider(self.groupBox_3)
        self.graph_node_h_spacing_slider.setObjectName(u"graph_node_h_spacing_slider")
        self.graph_node_h_spacing_slider.setGeometry(QRect(420, 10, 200, 41))
        self.label_20 = QLabel(self.groupBox_3)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setGeometry(QRect(290, 50, 111, 20))
        self.label_34 = QLabel(self.groupBox_3)
        self.label_34.setObjectName(u"label_34")
        self.label_34.setGeometry(QRect(20, 90, 121, 17))
        self.graph_node_corner_rounding_slider = SettingsSlider(self.groupBox_3)
        self.graph_node_corner_rounding_slider.setObjectName(u"graph_node_corner_rounding_slider")
        self.graph_node_corner_rounding_slider.setGeometry(QRect(140, 80, 200, 41))
        self.graph_node_use_shadow_cb = QCheckBox(self.groupBox_3)
        self.graph_node_use_shadow_cb.setObjectName(u"graph_node_use_shadow_cb")
        self.graph_node_use_shadow_cb.setGeometry(QRect(180, 130, 94, 26))
        self.groupBox_4 = QGroupBox(self.graphs_tab)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.groupBox_4.setGeometry(QRect(10, 290, 461, 211))
        self.groupBox_4.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.graph_edge_arrow_size_slider = SettingsSlider(self.groupBox_4)
        self.graph_edge_arrow_size_slider.setObjectName(u"graph_edge_arrow_size_slider")
        self.graph_edge_arrow_size_slider.setGeometry(QRect(250, 20, 200, 41))
        self.label_31 = QLabel(self.groupBox_4)
        self.label_31.setObjectName(u"label_31")
        self.label_31.setGeometry(QRect(30, 30, 101, 20))
        self.label_32 = QLabel(self.groupBox_4)
        self.label_32.setObjectName(u"label_32")
        self.label_32.setGeometry(QRect(130, 130, 61, 20))
        self.graph_edge_color_pb = QPushButton(self.groupBox_4)
        self.graph_edge_color_pb.setObjectName(u"graph_edge_color_pb")
        self.graph_edge_color_pb.setGeometry(QRect(130, 150, 83, 41))
        self.label_67 = QLabel(self.groupBox_4)
        self.label_67.setObjectName(u"label_67")
        self.label_67.setGeometry(QRect(30, 60, 151, 18))
        self.label_68 = QLabel(self.groupBox_4)
        self.label_68.setObjectName(u"label_68")
        self.label_68.setGeometry(QRect(30, 90, 281, 18))
        self.graph_edge_annotation_width_slider = SettingsSlider(self.groupBox_4)
        self.graph_edge_annotation_width_slider.setObjectName(u"graph_edge_annotation_width_slider")
        self.graph_edge_annotation_width_slider.setGeometry(QRect(250, 50, 200, 41))
        self.graph_edge_annotation_background_opacity_slider = SettingsSliderPercent(self.groupBox_4)
        self.graph_edge_annotation_background_opacity_slider.setObjectName(u"graph_edge_annotation_background_opacity_slider")
        self.graph_edge_annotation_background_opacity_slider.setGeometry(QRect(250, 80, 200, 41))
        self.settings_widget.addTab(self.graphs_tab, "")
        self.code_editor_tab = QWidget()
        self.code_editor_tab.setObjectName(u"code_editor_tab")
        self.groupBox_13 = QGroupBox(self.code_editor_tab)
        self.groupBox_13.setObjectName(u"groupBox_13")
        self.groupBox_13.setGeometry(QRect(10, 10, 231, 101))
        self.groupBox_13.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.code_editor_keyword_color_pb = QPushButton(self.groupBox_13)
        self.code_editor_keyword_color_pb.setObjectName(u"code_editor_keyword_color_pb")
        self.code_editor_keyword_color_pb.setGeometry(QRect(20, 40, 83, 41))
        self.code_editor_meta_symbol_color_pb = QPushButton(self.groupBox_13)
        self.code_editor_meta_symbol_color_pb.setObjectName(u"code_editor_meta_symbol_color_pb")
        self.code_editor_meta_symbol_color_pb.setGeometry(QRect(120, 40, 83, 41))
        self.label_21 = QLabel(self.groupBox_13)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setGeometry(QRect(20, 20, 91, 20))
        self.label_29 = QLabel(self.groupBox_13)
        self.label_29.setObjectName(u"label_29")
        self.label_29.setGeometry(QRect(120, 20, 101, 20))
        self.groupBox_14 = QGroupBox(self.code_editor_tab)
        self.groupBox_14.setObjectName(u"groupBox_14")
        self.groupBox_14.setGeometry(QRect(10, 230, 321, 101))
        self.groupBox_14.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.code_editor_atomic_color_pb = QPushButton(self.groupBox_14)
        self.code_editor_atomic_color_pb.setObjectName(u"code_editor_atomic_color_pb")
        self.code_editor_atomic_color_pb.setGeometry(QRect(120, 40, 83, 41))
        self.label_54 = QLabel(self.groupBox_14)
        self.label_54.setObjectName(u"label_54")
        self.label_54.setGeometry(QRect(220, 20, 91, 20))
        self.code_editor_composite_color_pb = QPushButton(self.groupBox_14)
        self.code_editor_composite_color_pb.setObjectName(u"code_editor_composite_color_pb")
        self.code_editor_composite_color_pb.setGeometry(QRect(220, 40, 83, 41))
        self.code_editor_root_color_pb = QPushButton(self.groupBox_14)
        self.code_editor_root_color_pb.setObjectName(u"code_editor_root_color_pb")
        self.code_editor_root_color_pb.setGeometry(QRect(20, 40, 83, 41))
        self.label_38 = QLabel(self.groupBox_14)
        self.label_38.setObjectName(u"label_38")
        self.label_38.setGeometry(QRect(120, 20, 91, 20))
        self.label_47 = QLabel(self.groupBox_14)
        self.label_47.setObjectName(u"label_47")
        self.label_47.setGeometry(QRect(20, 20, 91, 20))
        self.groupBox_15 = QGroupBox(self.code_editor_tab)
        self.groupBox_15.setObjectName(u"groupBox_15")
        self.groupBox_15.setGeometry(QRect(10, 340, 521, 101))
        self.groupBox_15.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.code_editor_variable_color_pb = QPushButton(self.groupBox_15)
        self.code_editor_variable_color_pb.setObjectName(u"code_editor_variable_color_pb")
        self.code_editor_variable_color_pb.setGeometry(QRect(220, 40, 83, 41))
        self.code_editor_quoted_text_color_pb = QPushButton(self.groupBox_15)
        self.code_editor_quoted_text_color_pb.setObjectName(u"code_editor_quoted_text_color_pb")
        self.code_editor_quoted_text_color_pb.setGeometry(QRect(120, 40, 83, 41))
        self.code_editor_comment_color_pb = QPushButton(self.groupBox_15)
        self.code_editor_comment_color_pb.setObjectName(u"code_editor_comment_color_pb")
        self.code_editor_comment_color_pb.setGeometry(QRect(20, 40, 83, 41))
        self.label_26 = QLabel(self.groupBox_15)
        self.label_26.setObjectName(u"label_26")
        self.label_26.setGeometry(QRect(220, 20, 91, 20))
        self.label_28 = QLabel(self.groupBox_15)
        self.label_28.setObjectName(u"label_28")
        self.label_28.setGeometry(QRect(120, 20, 101, 20))
        self.label_19 = QLabel(self.groupBox_15)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setGeometry(QRect(20, 20, 91, 20))
        self.code_editor_operator_color_pb = QPushButton(self.groupBox_15)
        self.code_editor_operator_color_pb.setObjectName(u"code_editor_operator_color_pb")
        self.code_editor_operator_color_pb.setGeometry(QRect(420, 40, 83, 41))
        self.label_27 = QLabel(self.groupBox_15)
        self.label_27.setObjectName(u"label_27")
        self.label_27.setGeometry(QRect(420, 20, 91, 20))
        self.label_30 = QLabel(self.groupBox_15)
        self.label_30.setObjectName(u"label_30")
        self.label_30.setGeometry(QRect(320, 20, 91, 20))
        self.code_editor_number_color_pb = QPushButton(self.groupBox_15)
        self.code_editor_number_color_pb.setObjectName(u"code_editor_number_color_pb")
        self.code_editor_number_color_pb.setGeometry(QRect(320, 40, 83, 41))
        self.groupBox_16 = QGroupBox(self.code_editor_tab)
        self.groupBox_16.setObjectName(u"groupBox_16")
        self.groupBox_16.setGeometry(QRect(10, 120, 131, 101))
        self.groupBox_16.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.code_editor_schema_color_pb = QPushButton(self.groupBox_16)
        self.code_editor_schema_color_pb.setObjectName(u"code_editor_schema_color_pb")
        self.code_editor_schema_color_pb.setGeometry(QRect(20, 40, 83, 41))
        self.label_57 = QLabel(self.groupBox_16)
        self.label_57.setObjectName(u"label_57")
        self.label_57.setGeometry(QRect(20, 20, 91, 20))
        self.settings_widget.addTab(self.code_editor_tab, "")
        self.bar_charts_tab = QWidget()
        self.bar_charts_tab.setObjectName(u"bar_charts_tab")
        self.bar_chart_bar_length_slider = SettingsSlider(self.bar_charts_tab)
        self.bar_chart_bar_length_slider.setObjectName(u"bar_chart_bar_length_slider")
        self.bar_chart_bar_length_slider.setGeometry(QRect(160, 20, 200, 41))
        self.label_37 = QLabel(self.bar_charts_tab)
        self.label_37.setObjectName(u"label_37")
        self.label_37.setGeometry(QRect(30, 30, 81, 20))
        self.label_39 = QLabel(self.bar_charts_tab)
        self.label_39.setObjectName(u"label_39")
        self.label_39.setGeometry(QRect(30, 60, 101, 20))
        self.bar_chart_bar_thickness_slider = SettingsSlider(self.bar_charts_tab)
        self.bar_chart_bar_thickness_slider.setObjectName(u"bar_chart_bar_thickness_slider")
        self.bar_chart_bar_thickness_slider.setGeometry(QRect(160, 50, 200, 41))
        self.bar_chart_bar_gap_slider = SettingsSlider(self.bar_charts_tab)
        self.bar_chart_bar_gap_slider.setObjectName(u"bar_chart_bar_gap_slider")
        self.bar_chart_bar_gap_slider.setGeometry(QRect(160, 80, 200, 41))
        self.label_55 = QLabel(self.bar_charts_tab)
        self.label_55.setObjectName(u"label_55")
        self.label_55.setGeometry(QRect(30, 90, 131, 20))
        self.bar_chart_bar_use_border_cb = QCheckBox(self.bar_charts_tab)
        self.bar_chart_bar_use_border_cb.setObjectName(u"bar_chart_bar_use_border_cb")
        self.bar_chart_bar_use_border_cb.setGeometry(QRect(30, 130, 111, 23))
        self.groupBox_17 = QGroupBox(self.bar_charts_tab)
        self.groupBox_17.setObjectName(u"groupBox_17")
        self.groupBox_17.setGeometry(QRect(20, 210, 431, 181))
        self.groupBox_17.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.bar_chart_bar_color3_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color3_pb.setObjectName(u"bar_chart_bar_color3_pb")
        self.bar_chart_bar_color3_pb.setGeometry(QRect(220, 40, 83, 41))
        self.bar_chart_bar_color2_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color2_pb.setObjectName(u"bar_chart_bar_color2_pb")
        self.bar_chart_bar_color2_pb.setGeometry(QRect(120, 40, 83, 41))
        self.bar_chart_bar_color1_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color1_pb.setObjectName(u"bar_chart_bar_color1_pb")
        self.bar_chart_bar_color1_pb.setGeometry(QRect(20, 40, 83, 41))
        self.label_56 = QLabel(self.groupBox_17)
        self.label_56.setObjectName(u"label_56")
        self.label_56.setGeometry(QRect(220, 20, 91, 20))
        self.label_58 = QLabel(self.groupBox_17)
        self.label_58.setObjectName(u"label_58")
        self.label_58.setGeometry(QRect(120, 20, 101, 20))
        self.label_59 = QLabel(self.groupBox_17)
        self.label_59.setObjectName(u"label_59")
        self.label_59.setGeometry(QRect(20, 20, 91, 20))
        self.label_61 = QLabel(self.groupBox_17)
        self.label_61.setObjectName(u"label_61")
        self.label_61.setGeometry(QRect(320, 20, 91, 20))
        self.bar_chart_bar_color4_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color4_pb.setObjectName(u"bar_chart_bar_color4_pb")
        self.bar_chart_bar_color4_pb.setGeometry(QRect(320, 40, 83, 41))
        self.label_62 = QLabel(self.groupBox_17)
        self.label_62.setObjectName(u"label_62")
        self.label_62.setGeometry(QRect(320, 100, 91, 20))
        self.bar_chart_bar_color7_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color7_pb.setObjectName(u"bar_chart_bar_color7_pb")
        self.bar_chart_bar_color7_pb.setGeometry(QRect(220, 120, 83, 41))
        self.label_60 = QLabel(self.groupBox_17)
        self.label_60.setObjectName(u"label_60")
        self.label_60.setGeometry(QRect(220, 100, 91, 20))
        self.bar_chart_bar_color8_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color8_pb.setObjectName(u"bar_chart_bar_color8_pb")
        self.bar_chart_bar_color8_pb.setGeometry(QRect(320, 120, 83, 41))
        self.label_63 = QLabel(self.groupBox_17)
        self.label_63.setObjectName(u"label_63")
        self.label_63.setGeometry(QRect(120, 100, 101, 20))
        self.bar_chart_bar_color5_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color5_pb.setObjectName(u"bar_chart_bar_color5_pb")
        self.bar_chart_bar_color5_pb.setGeometry(QRect(20, 120, 83, 41))
        self.label_64 = QLabel(self.groupBox_17)
        self.label_64.setObjectName(u"label_64")
        self.label_64.setGeometry(QRect(20, 100, 91, 20))
        self.bar_chart_bar_color6_pb = QPushButton(self.groupBox_17)
        self.bar_chart_bar_color6_pb.setObjectName(u"bar_chart_bar_color6_pb")
        self.bar_chart_bar_color6_pb.setGeometry(QRect(120, 120, 83, 41))
        self.bar_chart_bar_annotation_decimal_places_slider = SettingsSlider(self.bar_charts_tab)
        self.bar_chart_bar_annotation_decimal_places_slider.setObjectName(u"bar_chart_bar_annotation_decimal_places_slider")
        self.bar_chart_bar_annotation_decimal_places_slider.setGeometry(QRect(250, 160, 200, 41))
        self.label_69 = QLabel(self.bar_charts_tab)
        self.label_69.setObjectName(u"label_69")
        self.label_69.setGeometry(QRect(30, 170, 231, 20))
        self.settings_widget.addTab(self.bar_charts_tab, "")
        self.activity_diagrams_tab = QWidget()
        self.activity_diagrams_tab.setObjectName(u"activity_diagrams_tab")
        self.groupBox_6 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_6.setObjectName(u"groupBox_6")
        self.groupBox_6.setGeometry(QRect(10, 120, 311, 211))
        self.groupBox_6.setLayoutDirection(Qt.LeftToRight)
        self.groupBox_6.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.ad_action_color_pb = QPushButton(self.groupBox_6)
        self.ad_action_color_pb.setObjectName(u"ad_action_color_pb")
        self.ad_action_color_pb.setGeometry(QRect(20, 120, 83, 41))
        self.ad_action_color_gradient_slider = SettingsSliderPercent(self.groupBox_6)
        self.ad_action_color_gradient_slider.setObjectName(u"ad_action_color_gradient_slider")
        self.ad_action_color_gradient_slider.setGeometry(QRect(90, 160, 200, 41))
        self.label_40 = QLabel(self.groupBox_6)
        self.label_40.setObjectName(u"label_40")
        self.label_40.setGeometry(QRect(20, 170, 71, 31))
        self.label_41 = QLabel(self.groupBox_6)
        self.label_41.setObjectName(u"label_41")
        self.label_41.setGeometry(QRect(20, 60, 51, 20))
        self.label_42 = QLabel(self.groupBox_6)
        self.label_42.setObjectName(u"label_42")
        self.label_42.setGeometry(QRect(20, 30, 51, 20))
        self.ad_action_width_slider = SettingsSlider(self.groupBox_6)
        self.ad_action_width_slider.setObjectName(u"ad_action_width_slider")
        self.ad_action_width_slider.setGeometry(QRect(90, 20, 200, 41))
        self.ad_action_height_slider = SettingsSlider(self.groupBox_6)
        self.ad_action_height_slider.setObjectName(u"ad_action_height_slider")
        self.ad_action_height_slider.setGeometry(QRect(90, 50, 200, 41))
        self.label_45 = QLabel(self.groupBox_6)
        self.label_45.setObjectName(u"label_45")
        self.label_45.setGeometry(QRect(20, 90, 71, 20))
        self.ad_action_corner_rounding_slider = SettingsSlider(self.groupBox_6)
        self.ad_action_corner_rounding_slider.setObjectName(u"ad_action_corner_rounding_slider")
        self.ad_action_corner_rounding_slider.setGeometry(QRect(90, 80, 200, 41))
        self.ad_action_use_border_cb = QCheckBox(self.groupBox_6)
        self.ad_action_use_border_cb.setObjectName(u"ad_action_use_border_cb")
        self.ad_action_use_border_cb.setGeometry(QRect(190, 130, 81, 23))
        self.groupBox_7 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_7.setObjectName(u"groupBox_7")
        self.groupBox_7.setGeometry(QRect(10, 340, 311, 121))
        self.groupBox_7.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.ad_edge_arrow_size_slider = SettingsSlider(self.groupBox_7)
        self.ad_edge_arrow_size_slider.setObjectName(u"ad_edge_arrow_size_slider")
        self.ad_edge_arrow_size_slider.setGeometry(QRect(100, 20, 200, 41))
        self.label_46 = QLabel(self.groupBox_7)
        self.label_46.setObjectName(u"label_46")
        self.label_46.setGeometry(QRect(20, 30, 101, 20))
        self.ad_edge_color_pb = QPushButton(self.groupBox_7)
        self.ad_edge_color_pb.setObjectName(u"ad_edge_color_pb")
        self.ad_edge_color_pb.setGeometry(QRect(20, 60, 83, 41))
        self.groupBox_8 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_8.setObjectName(u"groupBox_8")
        self.groupBox_8.setGeometry(QRect(10, 10, 311, 101))
        self.groupBox_8.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.label_44 = QLabel(self.groupBox_8)
        self.label_44.setObjectName(u"label_44")
        self.label_44.setGeometry(QRect(20, 60, 61, 20))
        self.label_43 = QLabel(self.groupBox_8)
        self.label_43.setObjectName(u"label_43")
        self.label_43.setGeometry(QRect(20, 30, 81, 20))
        self.ad_v_spacing_slider = SettingsSlider(self.groupBox_8)
        self.ad_v_spacing_slider.setObjectName(u"ad_v_spacing_slider")
        self.ad_v_spacing_slider.setGeometry(QRect(100, 50, 200, 41))
        self.ad_h_spacing_slider = SettingsSlider(self.groupBox_8)
        self.ad_h_spacing_slider.setObjectName(u"ad_h_spacing_slider")
        self.ad_h_spacing_slider.setGeometry(QRect(100, 20, 200, 41))
        self.groupBox_9 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_9.setObjectName(u"groupBox_9")
        self.groupBox_9.setGeometry(QRect(350, 230, 271, 131))
        self.groupBox_9.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.ad_decision_width_slider = SettingsSlider(self.groupBox_9)
        self.ad_decision_width_slider.setObjectName(u"ad_decision_width_slider")
        self.ad_decision_width_slider.setGeometry(QRect(60, 10, 200, 41))
        self.label_48 = QLabel(self.groupBox_9)
        self.label_48.setObjectName(u"label_48")
        self.label_48.setGeometry(QRect(10, 20, 51, 20))
        self.ad_decision_color_pb = QPushButton(self.groupBox_9)
        self.ad_decision_color_pb.setObjectName(u"ad_decision_color_pb")
        self.ad_decision_color_pb.setGeometry(QRect(20, 80, 83, 41))
        self.ad_decision_height_slider = SettingsSlider(self.groupBox_9)
        self.ad_decision_height_slider.setObjectName(u"ad_decision_height_slider")
        self.ad_decision_height_slider.setGeometry(QRect(60, 40, 200, 41))
        self.label_51 = QLabel(self.groupBox_9)
        self.label_51.setObjectName(u"label_51")
        self.label_51.setGeometry(QRect(10, 50, 51, 20))
        self.ad_decision_use_border_cb = QCheckBox(self.groupBox_9)
        self.ad_decision_use_border_cb.setObjectName(u"ad_decision_use_border_cb")
        self.ad_decision_use_border_cb.setGeometry(QRect(160, 90, 81, 23))
        self.groupBox_10 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_10.setObjectName(u"groupBox_10")
        self.groupBox_10.setGeometry(QRect(350, 10, 271, 101))
        self.groupBox_10.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.ad_start_size_slider = SettingsSlider(self.groupBox_10)
        self.ad_start_size_slider.setObjectName(u"ad_start_size_slider")
        self.ad_start_size_slider.setGeometry(QRect(60, 10, 200, 41))
        self.label_49 = QLabel(self.groupBox_10)
        self.label_49.setObjectName(u"label_49")
        self.label_49.setGeometry(QRect(20, 20, 41, 20))
        self.ad_start_use_border_cb = QCheckBox(self.groupBox_10)
        self.ad_start_use_border_cb.setObjectName(u"ad_start_use_border_cb")
        self.ad_start_use_border_cb.setGeometry(QRect(160, 60, 81, 23))
        self.ad_start_color_pb = QPushButton(self.groupBox_10)
        self.ad_start_color_pb.setObjectName(u"ad_start_color_pb")
        self.ad_start_color_pb.setGeometry(QRect(20, 50, 83, 41))
        self.groupBox_11 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_11.setObjectName(u"groupBox_11")
        self.groupBox_11.setGeometry(QRect(350, 120, 271, 101))
        self.groupBox_11.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.ad_end_size_slider = SettingsSlider(self.groupBox_11)
        self.ad_end_size_slider.setObjectName(u"ad_end_size_slider")
        self.ad_end_size_slider.setGeometry(QRect(60, 10, 200, 41))
        self.label_50 = QLabel(self.groupBox_11)
        self.label_50.setObjectName(u"label_50")
        self.label_50.setGeometry(QRect(20, 20, 41, 20))
        self.ad_end_use_border_cb = QCheckBox(self.groupBox_11)
        self.ad_end_use_border_cb.setObjectName(u"ad_end_use_border_cb")
        self.ad_end_use_border_cb.setGeometry(QRect(160, 60, 81, 23))
        self.ad_end_color_pb = QPushButton(self.groupBox_11)
        self.ad_end_color_pb.setObjectName(u"ad_end_color_pb")
        self.ad_end_color_pb.setGeometry(QRect(20, 50, 83, 41))
        self.groupBox_12 = QGroupBox(self.activity_diagrams_tab)
        self.groupBox_12.setObjectName(u"groupBox_12")
        self.groupBox_12.setGeometry(QRect(350, 370, 271, 131))
        self.groupBox_12.setStyleSheet(u"QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.ad_bar_width_slider = SettingsSlider(self.groupBox_12)
        self.ad_bar_width_slider.setObjectName(u"ad_bar_width_slider")
        self.ad_bar_width_slider.setGeometry(QRect(60, 10, 200, 41))
        self.label_52 = QLabel(self.groupBox_12)
        self.label_52.setObjectName(u"label_52")
        self.label_52.setGeometry(QRect(10, 20, 51, 20))
        self.ad_bar_color_pb = QPushButton(self.groupBox_12)
        self.ad_bar_color_pb.setObjectName(u"ad_bar_color_pb")
        self.ad_bar_color_pb.setGeometry(QRect(20, 80, 83, 41))
        self.ad_bar_height_slider = SettingsSlider(self.groupBox_12)
        self.ad_bar_height_slider.setObjectName(u"ad_bar_height_slider")
        self.ad_bar_height_slider.setGeometry(QRect(60, 40, 200, 41))
        self.label_53 = QLabel(self.groupBox_12)
        self.label_53.setObjectName(u"label_53")
        self.label_53.setGeometry(QRect(10, 50, 51, 20))
        self.ad_bar_use_border_cb = QCheckBox(self.groupBox_12)
        self.ad_bar_use_border_cb.setObjectName(u"ad_bar_use_border_cb")
        self.ad_bar_use_border_cb.setGeometry(QRect(160, 90, 81, 23))
        self.settings_widget.addTab(self.activity_diagrams_tab, "")
        self.titles_tab = QWidget()
        self.titles_tab.setObjectName(u"titles_tab")
        self.label_35 = QLabel(self.titles_tab)
        self.label_35.setObjectName(u"label_35")
        self.label_35.setGeometry(QRect(20, 20, 571, 18))
        self.label_36 = QLabel(self.titles_tab)
        self.label_36.setObjectName(u"label_36")
        self.label_36.setGeometry(QRect(20, 60, 141, 18))
        self.title_width_slider = SettingsSlider(self.titles_tab)
        self.title_width_slider.setObjectName(u"title_width_slider")
        self.title_width_slider.setGeometry(QRect(100, 50, 200, 41))
        self.settings_widget.addTab(self.titles_tab, "")
        self.button_box = QDialogButtonBox(SettingsDialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setGeometry(QRect(270, 580, 91, 32))
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Close)
        self.button_box.setCenterButtons(False)

        self.retranslateUi(SettingsDialog)

        self.settings_widget.setCurrentIndex(3)


        QMetaObject.connectSlotsByName(SettingsDialog)
    # setupUi

    def retranslateUi(self, SettingsDialog):
        SettingsDialog.setWindowTitle(QCoreApplication.translate("SettingsDialog", u"Custom Theme", None))
        self.label_129.setText(QCoreApplication.translate("SettingsDialog", u"Export options configure how graph views are exported.", None))
        self.export_header_is_hidden_cb.setText(QCoreApplication.translate("SettingsDialog", u"Hide header", None))
        self.export_background_is_transparent_cb.setText(QCoreApplication.translate("SettingsDialog", u"Make background transparent (.png and .svg only)", None))
        self.label_70.setText(QCoreApplication.translate("SettingsDialog", u"Header vertical spacing", None))
        self.groupBox_18.setTitle(QCoreApplication.translate("SettingsDialog", u"Image format", None))
        self.export_image_format_bmp_rb.setText(QCoreApplication.translate("SettingsDialog", u".bmp", None))
        self.export_image_format_jpg_rb.setText(QCoreApplication.translate("SettingsDialog", u".jpg", None))
        self.export_image_format_pdf_rb.setText(QCoreApplication.translate("SettingsDialog", u".pdf", None))
        self.export_image_format_png_rb.setText(QCoreApplication.translate("SettingsDialog", u".png", None))
        self.export_image_format_svg_rb.setText(QCoreApplication.translate("SettingsDialog", u".svg", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.export_tab), QCoreApplication.translate("SettingsDialog", u"Exported Views", None))
        self.label.setText(QCoreApplication.translate("SettingsDialog", u"Color", None))
        self.background_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_15.setText(QCoreApplication.translate("SettingsDialog", u"Color gradient", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.background_tab), QCoreApplication.translate("SettingsDialog", u"Background", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("SettingsDialog", u"Draw borders", None))
        self.generation_charts_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Charts", None))
        self.generation_graphs_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Graphs", None))
        self.generation_reports_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Reports", None))
        self.generation_traces_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Traces", None))
        self.generation_background_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Background", None))
        self.generation_tables_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Tables", None))
        self.generation_ad_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Activity diagrams", None))
        self.label_33.setText(QCoreApplication.translate("SettingsDialog", u"Generation options configure which elements will have borders\n"
"when the trace-generator runs.", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.generation_tab), QCoreApplication.translate("SettingsDialog", u"Generation", None))
        self.groupBox.setTitle(QCoreApplication.translate("SettingsDialog", u"Nodes", None))
        self.label_5.setText(QCoreApplication.translate("SettingsDialog", u"Root", None))
        self.label_6.setText(QCoreApplication.translate("SettingsDialog", u"Atomic", None))
        self.label_7.setText(QCoreApplication.translate("SettingsDialog", u"Composite", None))
        self.label_8.setText(QCoreApplication.translate("SettingsDialog", u"Schema", None))
        self.label_9.setText(QCoreApplication.translate("SettingsDialog", u"Say", None))
        self.trace_node_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.trace_node_use_shadow_cb.setText(QCoreApplication.translate("SettingsDialog", u"Shadow", None))
        self.trace_node_root_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.trace_node_atomic_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.trace_node_composite_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.trace_node_schema_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.trace_node_say_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_14.setText(QCoreApplication.translate("SettingsDialog", u"Color gradient", None))
        self.label_4.setText(QCoreApplication.translate("SettingsDialog", u"Height", None))
        self.label_3.setText(QCoreApplication.translate("SettingsDialog", u"Width", None))
        self.label_11.setText(QCoreApplication.translate("SettingsDialog", u"Vertical spacing", None))
        self.label_12.setText(QCoreApplication.translate("SettingsDialog", u"Horizontal spacing", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("SettingsDialog", u"Edges", None))
        self.label_23.setText(QCoreApplication.translate("SettingsDialog", u"Encloses", None))
        self.label_24.setText(QCoreApplication.translate("SettingsDialog", u"Precedes", None))
        self.label_25.setText(QCoreApplication.translate("SettingsDialog", u"User Defined", None))
        self.trace_edge_in_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.trace_edge_follows_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.trace_edge_user_defined_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_22.setText(QCoreApplication.translate("SettingsDialog", u"Arrow size", None))
        self.label_65.setText(QCoreApplication.translate("SettingsDialog", u"Line label width", None))
        self.label_66.setText(QCoreApplication.translate("SettingsDialog", u"Line label background opacity", None))
        self.label_2.setText(QCoreApplication.translate("SettingsDialog", u"Hide/collapse opacity", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.traces_tab), QCoreApplication.translate("SettingsDialog", u"Traces", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("SettingsDialog", u"Nodes", None))
        self.label_10.setText(QCoreApplication.translate("SettingsDialog", u"Color", None))
        self.graph_node_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.graph_node_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_17.setText(QCoreApplication.translate("SettingsDialog", u"Color Gradient", None))
        self.label_13.setText(QCoreApplication.translate("SettingsDialog", u"Height", None))
        self.label_16.setText(QCoreApplication.translate("SettingsDialog", u"Width", None))
        self.label_18.setText(QCoreApplication.translate("SettingsDialog", u"Horizontal spacing", None))
        self.label_20.setText(QCoreApplication.translate("SettingsDialog", u"Vertical spacing", None))
        self.label_34.setText(QCoreApplication.translate("SettingsDialog", u"Corner rounding", None))
        self.graph_node_use_shadow_cb.setText(QCoreApplication.translate("SettingsDialog", u"Shadow", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("SettingsDialog", u"Edges", None))
        self.label_31.setText(QCoreApplication.translate("SettingsDialog", u"Arrow size", None))
        self.label_32.setText(QCoreApplication.translate("SettingsDialog", u"Color", None))
        self.graph_edge_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_67.setText(QCoreApplication.translate("SettingsDialog", u"Line label width", None))
        self.label_68.setText(QCoreApplication.translate("SettingsDialog", u"Line label background opacity", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.graphs_tab), QCoreApplication.translate("SettingsDialog", u"Graphs", None))
        self.groupBox_13.setTitle(QCoreApplication.translate("SettingsDialog", u"MP Language", None))
        self.code_editor_keyword_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.code_editor_meta_symbol_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_21.setText(QCoreApplication.translate("SettingsDialog", u"Keywords", None))
        self.label_29.setText(QCoreApplication.translate("SettingsDialog", u"Meta-Symbols", None))
        self.groupBox_14.setTitle(QCoreApplication.translate("SettingsDialog", u"Event types", None))
        self.code_editor_atomic_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_54.setText(QCoreApplication.translate("SettingsDialog", u"Composite", None))
        self.code_editor_composite_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.code_editor_root_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_38.setText(QCoreApplication.translate("SettingsDialog", u"Atomic", None))
        self.label_47.setText(QCoreApplication.translate("SettingsDialog", u"Root", None))
        self.groupBox_15.setTitle(QCoreApplication.translate("SettingsDialog", u"Other", None))
        self.code_editor_variable_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.code_editor_quoted_text_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.code_editor_comment_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_26.setText(QCoreApplication.translate("SettingsDialog", u"Variables", None))
        self.label_28.setText(QCoreApplication.translate("SettingsDialog", u"Quoted text", None))
        self.label_19.setText(QCoreApplication.translate("SettingsDialog", u"Comments", None))
        self.code_editor_operator_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_27.setText(QCoreApplication.translate("SettingsDialog", u"Operators", None))
        self.label_30.setText(QCoreApplication.translate("SettingsDialog", u"Numbers", None))
        self.code_editor_number_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.groupBox_16.setTitle(QCoreApplication.translate("SettingsDialog", u"Schema", None))
        self.code_editor_schema_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_57.setText(QCoreApplication.translate("SettingsDialog", u"Schema", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.code_editor_tab), QCoreApplication.translate("SettingsDialog", u"Code Editor", None))
        self.label_37.setText(QCoreApplication.translate("SettingsDialog", u"Bar length", None))
        self.label_39.setText(QCoreApplication.translate("SettingsDialog", u"Bar thickness", None))
        self.label_55.setText(QCoreApplication.translate("SettingsDialog", u"Gap between bars", None))
        self.bar_chart_bar_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Bar borders", None))
        self.groupBox_17.setTitle(QCoreApplication.translate("SettingsDialog", u"Bar colors", None))
        self.bar_chart_bar_color3_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.bar_chart_bar_color2_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.bar_chart_bar_color1_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_56.setText(QCoreApplication.translate("SettingsDialog", u"3", None))
        self.label_58.setText(QCoreApplication.translate("SettingsDialog", u"2", None))
        self.label_59.setText(QCoreApplication.translate("SettingsDialog", u"1", None))
        self.label_61.setText(QCoreApplication.translate("SettingsDialog", u"4", None))
        self.bar_chart_bar_color4_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_62.setText(QCoreApplication.translate("SettingsDialog", u"8", None))
        self.bar_chart_bar_color7_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_60.setText(QCoreApplication.translate("SettingsDialog", u"7", None))
        self.bar_chart_bar_color8_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_63.setText(QCoreApplication.translate("SettingsDialog", u"6", None))
        self.bar_chart_bar_color5_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_64.setText(QCoreApplication.translate("SettingsDialog", u"5", None))
        self.bar_chart_bar_color6_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_69.setText(QCoreApplication.translate("SettingsDialog", u"Bar annotation decimal places", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.bar_charts_tab), QCoreApplication.translate("SettingsDialog", u"Bar Charts", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("SettingsDialog", u"Actions", None))
        self.ad_action_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_40.setText(QCoreApplication.translate("SettingsDialog", u"Gradient", None))
        self.label_41.setText(QCoreApplication.translate("SettingsDialog", u"Height", None))
        self.label_42.setText(QCoreApplication.translate("SettingsDialog", u"Width", None))
        self.label_45.setText(QCoreApplication.translate("SettingsDialog", u"Rounding", None))
        self.ad_action_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.groupBox_7.setTitle(QCoreApplication.translate("SettingsDialog", u"Edges", None))
        self.label_46.setText(QCoreApplication.translate("SettingsDialog", u"Arrow size", None))
        self.ad_edge_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.groupBox_8.setTitle(QCoreApplication.translate("SettingsDialog", u"Spacing", None))
        self.label_44.setText(QCoreApplication.translate("SettingsDialog", u"Vertical", None))
        self.label_43.setText(QCoreApplication.translate("SettingsDialog", u"Horizontal", None))
        self.groupBox_9.setTitle(QCoreApplication.translate("SettingsDialog", u"Decision", None))
        self.label_48.setText(QCoreApplication.translate("SettingsDialog", u"Width", None))
        self.ad_decision_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_51.setText(QCoreApplication.translate("SettingsDialog", u"Height", None))
        self.ad_decision_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.groupBox_10.setTitle(QCoreApplication.translate("SettingsDialog", u"Start", None))
        self.label_49.setText(QCoreApplication.translate("SettingsDialog", u"Size", None))
        self.ad_start_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.ad_start_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.groupBox_11.setTitle(QCoreApplication.translate("SettingsDialog", u"End", None))
        self.label_50.setText(QCoreApplication.translate("SettingsDialog", u"Size", None))
        self.ad_end_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.ad_end_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.groupBox_12.setTitle(QCoreApplication.translate("SettingsDialog", u"Bar", None))
        self.label_52.setText(QCoreApplication.translate("SettingsDialog", u"Width", None))
        self.ad_bar_color_pb.setText(QCoreApplication.translate("SettingsDialog", u"#ffffff", None))
        self.label_53.setText(QCoreApplication.translate("SettingsDialog", u"Height", None))
        self.ad_bar_use_border_cb.setText(QCoreApplication.translate("SettingsDialog", u"Border", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.activity_diagrams_tab), QCoreApplication.translate("SettingsDialog", u"Activity Diagrams", None))
        self.label_35.setText(QCoreApplication.translate("SettingsDialog", u"Graphs, bar charts, Gantt charts and activity diagrams use titles.", None))
        self.label_36.setText(QCoreApplication.translate("SettingsDialog", u"Title width", None))
        self.settings_widget.setTabText(self.settings_widget.indexOf(self.titles_tab), QCoreApplication.translate("SettingsDialog", u"Titles", None))
    # retranslateUi

