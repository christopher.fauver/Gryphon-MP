# This makefile generates the following:
# * .py from .ui built using QT Designer.  We archive the generated
#   .py file so that users do not need to install pyuic5 and do not
#   need to rebuild .py from .ui.
# * version_file.py to match VERSION defined here.  We archive
#   version_file.py so users don't have to type "make" first.
# * The resources file which we also archive.
# * Executable distributions for Mac.
#
# You do not need to run this makefile unless you:
# * change a .ui file.
# * change the VERSION.
# * change resources.
# * want to install trace-generator and run tests

VERSION = v1.3.0_beta_3
DIST_VERSION = v1_3_0_beta_3
MAC_M1_APP = MP-Gryphon_mac_m1_$(DIST_VERSION)
MAC_INTEL_APP = MP-Gryphon_mac_intel_$(DIST_VERSION)
UBUNTU_X64_APP = MP-Gryphon_ubuntu_20_4_x64_$(DIST_VERSION)

ARCHIVED_GENERATED_UI_FILES = \
	settings_dialog.py about_mp_dialog.py \
	session_snapshots_dialog.py find_and_replace_dialog.py \
	search_mp_files_dialog.py search_mp_code_dialog.py \
	filter_event_dialog.py spellcheck_whitelist_dialog.py

.SUFFIXES: .ui .py


.ui.py:
	pyside6-uic $< > $@

all: version_file.py resources_rc.py $(ARCHIVED_GENERATED_UI_FILES)

version_file.py: Makefile
	@echo Updating version_file.py to $(VERSION)
	@echo "# This file is auto-generated by Makefile.  Do not edit this file." > version_file.py
	@echo "VERSION = \"$(VERSION)\"" >> version_file.py

resources_rc.py: resources.qrc \
	resources/icons/* resources/images/* resources/settings/* \
	resources/spellcheck_whitelist.txt
	pyside6-rcc resources.qrc -o resources_rc.py

# For win_app: Type 'pyinstaller MP-Gryphon.spec' and move to rename.
# Then build the installer: Run InstallForge, open windows_installer.ifp,
# adjust version numbers, click Build.

mac_m1_app:
	pyinstaller MP-Gryphon.spec
	mv dist/MP-Gryphon dist/$(MAC_M1_APP)

mac_intel_app:
	pyinstaller MP-Gryphon.spec
	mv dist/MP-Gryphon dist/$(MAC_INTEL_APP)

ubuntu_app:
	pyinstaller MP-Gryphon.spec
	mv dist/MP-Gryphon dist/$(UBUNTU_X64_APP)

doxy:
	doxygen ../doc/doxygen/Doxyfile

check:
	./test.py --skip_bad Example17_Dining_Philosophers.mp

clean:
	rm -f $(ARCHIVED_GENERATED_UI_FILES) resources_rc.py

