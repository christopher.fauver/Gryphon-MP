# Gryphon GUI

Gryphon provides a Python GUI user interface for accessing the Monterey Phoenix trace-generator.  Gryphon is available for Windows, Mac, and Linux.  On Linux systems, Gryphon runs stand-alone without requiring access to the Gryphon server.

* [Gryphon Wiki](https://gitlab.nps.edu/monterey-phoenix/user-interfaces/gryphon/-/wikis/home)
* [Installing MP Gryphon](https://gitlab.nps.edu/monterey-phoenix/user-interfaces/gryphon/-/wikis/Installing-MP-Gryphon)

