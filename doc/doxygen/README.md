File `Doxyfile` was generated using the `doxywizard` tool.  It is currently configured to generate HTML.  `doxygen` may be run from the Makefile:

    cd ~/gitlab_mp/gryphon/python
    make doxy

Output is placed at `~/gitlab_mp/gryphon/doc/doxygen/html`.  Example usage:

    cd ~/gitlab_mp/gryphon/python
    firefox ../doc/doxygen/html/classes.html 

